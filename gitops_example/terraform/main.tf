provider "aws" {
  region = "eu-central-1"
}


module "gitops_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "gitops-demo"
  cidr = "172.18.0.0/18"

  azs = [
    "eu-central-1a",
    "eu-central-1b",
    "eu-central-1c"]
  public_subnets = [
    "172.18.0.0/24",
    "172.18.1.0/24",
    "172.18.2.0/24"]

  enable_nat_gateway = false
  single_nat_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }

  vpc_tags = {
    "kubernetes.io/cluster/gitops_demo" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
    "kubernetes.io/cluster/gitops_demo" = "shared"
  }
}


module "gitops_cluster" {
  source = "terraform-aws-modules/eks/aws"
  cluster_name = "gitops_demo"
  subnets = module.gitops_vpc.public_subnets
  vpc_id = "${module.gitops_vpc.vpc_id}"

  cluster_endpoint_private_access = "true"
  cluster_endpoint_public_access = "true"

  worker_groups = [
    {
      name = "t3.medium spot private"
      instance_type = "t3.medium"
      subnets = module.gitops_vpc.public_subnets
      asg_min_size = 1
      asg_max_size = 2
      root_volume_size = 30
      spot_price = "0.1"
    },
  ]
}

